

const filterStep = 12;
const grid = document.querySelector('.pictures-grid');
let categoriesItems = document.querySelectorAll('.categories-item');

const pictures = [
    {
        src: 'assets/img/graphic design/graphic-design1.jpg',
        category: 'graphicDesign',
    },
    {
        src: 'assets/img/graphic design/graphic-design2.jpg',
        category: 'graphicDesign',
    },
    {
        src: 'assets/img/graphic design/graphic-design3.jpg',
        category: 'graphicDesign',
    },
    {
        src: 'assets/img/graphic design/graphic-design4.jpg',
        category: 'graphicDesign',
    },
    {
        src: 'assets/img/graphic design/graphic-design5.jpg',
        category: 'graphicDesign',
    },
    {
        src: 'assets/img/graphic design/graphic-design6.jpg',
        category: 'graphicDesign',
    },
    {
        src: 'assets/img/graphic design/graphic-design7.jpg',
        category: 'graphicDesign',
    },
    {
        src: 'assets/img/graphic design/graphic-design8.jpg',
        category: 'graphicDesign',
    },
    {
        src: 'assets/img/graphic design/graphic-design9.jpg',
        category: 'graphicDesign',
    },
    {
        src: 'assets/img/graphic design/graphic-design10.jpg',
        category: 'graphicDesign',
    },
    {
        src: 'assets/img/graphic design/graphic-design11.jpg',
        category: 'graphicDesign',
    },
    {
        src: 'assets/img/graphic design/graphic-design12.jpg',
        category: 'graphicDesign',
    },
    {
        src: 'assets/img/landing page/landing-page1.jpg',
        category: 'landingPages',
    },
    {
        src: 'assets/img/landing page/landing-page2.jpg',
        category: 'landingPages',
    },
    {
        src: 'assets/img/landing page/landing-page3.jpg',
        category: 'landingPages',
    },
    {
        src: 'assets/img/landing page/landing-page4.jpg',
        category: 'landingPages',
    },
    {
        src: 'assets/img/landing page/landing-page5.jpg',
        category: 'landingPages',
    },
    {
        src: 'assets/img/landing page/landing-page6.jpg',
        category: 'landingPages',
    },
    {
        src: 'assets/img/landing page/landing-page7.jpg',
        category: 'landingPages',
    },
    {
        src: 'assets/img/web design/web-design1.jpg',
        category: 'webDesignt',
    },
    {
        src: 'assets/img/web design/web-design2.jpg',
        category: 'webDesignt',
    },
    {
        src: 'assets/img/web design/web-design3.jpg',
        category: 'webDesignt',
    },
    {
        src: 'assets/img/web design/web-design4.jpg',
        category: 'webDesignt',
    },
    {
        src: 'assets/img/web design/web-design5.jpg',
        category: 'webDesignt',
    },
    {
        src: 'assets/img/web design/web-design6.jpg',
        category: 'webDesignt',
    },
    {
        src: 'assets/img/web design/web-design7.jpg',
        category: 'webDesignt',
    },
    {
        src: 'assets/img/wordpress/wordpress1.jpg',
        category: 'wordpress',
    },
    {
        src: 'assets/img/wordpress/wordpress2.jpg',
        category: 'wordpress',
    },
    {
        src: 'assets/img/wordpress/wordpress3.jpg',
        category: 'wordpress',
    },
    {
        src: 'assets/img/wordpress/wordpress4.jpg',
        category: 'wordpress',
    },
    {
        src: 'assets/img/wordpress/wordpress5.jpg',
        category: 'wordpress',
    },
    {
        src: 'assets/img/wordpress/wordpress6.jpg',
        category: 'wordpress',
    },
    {
        src: 'assets/img/wordpress/wordpress7.jpg',
        category: 'wordpress',
    },
    {
        src: 'assets/img/wordpress/wordpress8.jpg',
        category: 'wordpress',
    },
    {
        src: 'assets/img/wordpress/wordpress9.jpg',
        category: 'wordpress',
    },
    {
        src: 'assets/img/wordpress/wordpress10.jpg',
        category: 'wordpress',
    },

];

const button = document.querySelector('.btn-load-more');
button.addEventListener('click', function () {
    let currentCategory = document.querySelector('.categories-item.active');
    createImages(currentCategory.getAttribute('data-category'));
})

categoriesItems.forEach(categoriesItem => {

	categoriesItem.addEventListener('click', () => {
		
        categoriesItems.forEach(item => {
            item.classList.remove('active');
        });

		categoriesItem.classList.add('active');
        let selected = categoriesItem.getAttribute('data-category');
        grid.innerHTML = ""
        button.setAttribute('data-count',  0);
        createImages(selected);


    })
});


function createImages(category) {
    let countPictures = button.getAttribute('data-count');
    let startCount = countPictures;
    let finishCount = +countPictures + +filterStep;
    let arrayCounter = 0;

    pictures.forEach( picture => {
        if(category === 'all' || picture.category === category) {
            
            if(arrayCounter >= startCount && arrayCounter < finishCount) {
                let img = document.createElement('img');
                img.src = picture.src;
                grid.appendChild(img);
                startCount++;
            }

            arrayCounter++

        }
    });

    if(startCount == pictures.length || startCount < (finishCount - 1) || arrayCounter == startCount) {
        startCount = 0;
        button.style.display = "none";
    } else {
        button.style.display = "block";
    }
    button.setAttribute('data-count',  startCount);
}

createImages('all');

