const tabsTitle = document.querySelectorAll('.tabs-title');
const tabsContent = document.querySelectorAll('.tabs-content-item');


    tabsTitle.forEach((tab, index) => {
        tab.addEventListener('click', () => {
            TabContent();
            showTabContent(index);
        });
    });


    function TabContent() {
        tabsContent.forEach(item => {
            item.classList.remove('active');
        });

        tabsTitle.forEach(item => {
            item.classList.remove('active');
        });
    }

    function showTabContent(i = 0) {
        tabsContent[i].classList.add('active');
        tabsTitle[i].classList.add('active');
    }

    TabContent();
    showTabContent();
